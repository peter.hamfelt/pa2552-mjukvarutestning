# PA2552-Mjukvarutestning

## Getting started

### Development

1. Install python (download from web)
2. Install git (download from web)
3. Preferable IDE: PyCharm community 
4. Verify that ChromeDriver works from the command line: $ chromedrivers
5. create virtual env for system requirements (follow commands below)
```
$ cd PA2552-Mjukvarutestning
$ py -m venv venv
### Activate venv ###
$ source venv/scripts/activate
### Install requirements ###
$ pip install pytest
$ pip install selenium --dev
```
### Running all test modules

Running all test-case modules
```
--- REMINDER TO ACTIVATE VENV ---
$ cd PA2552-Mjukvarutestning
$ source venv/scripts/activate
--- REMINDER TO ACTIVATE VENV ---

$ pytest -s --disable-pytest-warnings --verbose --color=yes
```
### Running specific test module
```
$ pytest tests/test_math.py
```
### Running specific test case _(without deprecated warnings and with color)_
```
$ pytest tests/test_web.py::test_my_test-case -s --disable-pytest-warnings --verbose --color=yes
```
***

