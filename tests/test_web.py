"""
Tests use Selenium WebDriver with Chrome and ChromeDriver.
The fixtures set up and clean up the ChromeDriver instance.
"""
import pytest
import re
import time
from selenium.webdriver import Chrome


@pytest.fixture
def browser():
    # Start execution time counter
    start = time.time()
    print("Starting New Gui Test")

    # Initialize ChromeDriver
    driver = Chrome()
    # Wait implicitly for elements to be ready before attempting interactions
    driver.implicitly_wait(10)

    # Return the driver object at the end of setup
    yield driver

    # For cleanup, quit the driver
    driver.quit()

    # End execution time counter
    end = time.time()
    print(end - start)


def setup_url(browser):
    # website url
    url = 'https://www.saucedemo.com/'

    # navigate to url
    browser.get(url)

    return browser


def signin(browser):
    browser = setup_url(browser)

    # input correct username and password
    input_username_elem = browser.find_element_by_xpath('//*[@id="user-name"]')
    input_password_elem = browser.find_element_by_xpath('//*[@id="password"]')
    username_phrase = "standard_user"
    password_phrase = "secret_sauce"

    # Input and click on login
    input_username_elem.send_keys(username_phrase)
    input_password_elem.send_keys(password_phrase)
    browser.find_element_by_xpath('//*[@id="login-button"]').click()

    return browser


# TEST 1
def test_incorrect_signin(browser):
    """ Given """
    browser = setup_url(browser)

    # input incorrect username and password
    input_username_elem = browser.find_element_by_xpath('//*[@id="user-name"]')
    input_password_elem = browser.find_element_by_xpath('//*[@id="password"]')
    input_phrase = "incorrect123"

    """ When """
    # Input and click on login
    input_username_elem.send_keys(input_phrase)
    input_password_elem.send_keys(input_phrase)
    browser.find_element_by_xpath('//*[@id="login-button"]').click()

    """ Then """
    # Verify error message and that current url is still on login screen
    error_message = browser.find_element_by_xpath('//*[@id="login_button_container"]/div/form/div[3]/h3').text
    assert error_message == "Epic sadface: Username and password do not match any user in this service"
    assert browser.current_url == 'https://www.saucedemo.com/'


# TEST 2
def test_successful_signin(browser):
    """ Given """
    browser = setup_url(browser)

    # input correct username and password
    input_username_elem = browser.find_element_by_xpath('//*[@id="user-name"]')
    input_password_elem = browser.find_element_by_xpath('//*[@id="password"]')
    username_phrase = "standard_user"
    password_phrase = "secret_sauce"

    """ When """
    # Input and click on login
    input_username_elem.send_keys(username_phrase)
    input_password_elem.send_keys(password_phrase)
    browser.find_element_by_xpath('//*[@id="login-button"]').click()

    """ Then """
    # Verify current url is on index
    assert browser.current_url == 'https://www.saucedemo.com/inventory.html'


# TEST 3
def test_list_items_present(browser):
    """ Given """
    browser = signin(browser)

    """ When """
    get_nr_of_inventory_items = len(browser.find_elements_by_class_name('inventory_item'))

    """ Then """
    assert get_nr_of_inventory_items > 0
    assert get_nr_of_inventory_items == 6  # additional test to confirm the nr of current present items


# TEST 4
def test_add_items_to_cart(browser):
    """ Given """
    browser = signin(browser)

    """ When """
    browser.find_element_by_xpath('//*[@id="add-to-cart-sauce-labs-backpack"]').click()
    browser.find_element_by_xpath('//*[@id="add-to-cart-sauce-labs-bike-light"]').click()
    browser.find_element_by_xpath('//*[@id="shopping_cart_container"]/a').click()
    get_nr_of_cart_items = len(browser.find_elements_by_class_name('cart_item'))

    """ Then """
    assert get_nr_of_cart_items == 2


# TEST 5
def test_sorting_of_inventory_items_alphabetically(browser):
    """ Given """
    browser = signin(browser)

    """ When """
    # Sort A-Z
    browser.find_element_by_xpath('//*[@id="header_container"]/div[2]/div[2]/span/select/option[1]').click()
    az_sort = browser.find_elements_by_class_name('inventory_item_name')

    # Get text
    az_sort = [item.text for item in az_sort]
    # Build correct sort
    corrct_az_sort = [item for item in az_sort]
    corrct_az_sort.sort()

    # Sort Z-A
    browser.find_element_by_xpath('//*[@id="header_container"]/div[2]/div[2]/span/select/option[2]').click()
    za_sort = browser.find_elements_by_class_name('inventory_item_name')

    # Get text
    za_sort = [item.text for item in za_sort]
    # Build correct sort
    corrct_za_sort = [item for item in za_sort]
    corrct_za_sort.sort(key=None, reverse=True)

    """ Then """
    # Verify correct a-z sort
    assert za_sort == corrct_za_sort

    # Verify correct z-a sort
    assert za_sort == corrct_za_sort


# TEST 6
def test_sorting_of_inventory_items_price(browser):
    """ Given """
    browser = signin(browser)

    """ When """
    # Sort low to high
    browser.find_element_by_xpath('//*[@id="header_container"]/div[2]/div[2]/span/select/option[3]').click()
    low_high_sort = browser.find_elements_by_class_name('inventory_item_price')

    # Get text
    low_high_sort = [float(price.text[1:]) for price in low_high_sort]
    # Build correct sort
    correct_low_high_sort = [item for item in low_high_sort]
    correct_low_high_sort.sort()

    # Sort high to low
    browser.find_element_by_xpath('//*[@id="header_container"]/div[2]/div[2]/span/select/option[4]').click()
    high_low_sort = browser.find_elements_by_class_name('inventory_item_price')

    # Get text
    high_low_sort = [float(price.text[1:]) for price in high_low_sort]
    # Build correct sort
    correct_high_low_sort = [item for item in high_low_sort]
    correct_high_low_sort.sort(key=None, reverse=True)

    """ Then """
    # Verify correct low to high sort
    assert low_high_sort == correct_low_high_sort

    # Verify correct high to low sort
    assert high_low_sort == correct_high_low_sort


# TEST 7
def test_insufficient_checkout_information(browser):
    """ Given """
    browser = signin(browser)

    """ When """
    # Select and add items to cart
    browser.find_element_by_xpath('//*[@id="add-to-cart-sauce-labs-backpack"]').click()
    browser.find_element_by_xpath('//*[@id="add-to-cart-sauce-labs-bike-light"]').click()

    # View cart
    browser.find_element_by_xpath('//*[@id="shopping_cart_container"]/a').click()

    # Continue to checkout
    browser.find_element_by_xpath('//*[@id="checkout"]').click()

    # Submit checkout information
    browser.find_element_by_xpath('//*[@id="continue"]').click()

    # Get error message
    error_message = browser.find_element_by_xpath('//*[@id="checkout_info_container"]/div/form/div[1]/div[4]/h3').text

    """ Then """
    assert browser.current_url == 'https://www.saucedemo.com/checkout-step-one.html'
    assert error_message == 'Error: First Name is required'


# TEST 8
def test_successful_checkout(browser):
    """ Given """
    browser = signin(browser)

    first_name = "Peter"
    second_name = "Hamfelt"
    zip_nr = 1337

    """ When """
    # Select and add items to cart
    browser.find_element_by_xpath('//*[@id="add-to-cart-sauce-labs-backpack"]').click()
    browser.find_element_by_xpath('//*[@id="add-to-cart-sauce-labs-bike-light"]').click()

    # View cart
    browser.find_element_by_xpath('//*[@id="shopping_cart_container"]/a').click()

    # Continue to checkout
    browser.find_element_by_xpath('//*[@id="checkout"]').click()

    # Input checkout information
    browser.find_element_by_xpath('//*[@id="first-name"]').send_keys(first_name)
    browser.find_element_by_xpath('//*[@id="last-name"]').send_keys(second_name)
    browser.find_element_by_xpath('//*[@id="postal-code"]').send_keys(zip_nr)

    # Submit checkout information
    browser.find_element_by_xpath('//*[@id="continue"]').click()

    # Finish checkout overview
    browser.find_element_by_xpath('//*[@id="finish"]').click()

    """ Then """
    assert browser.current_url == 'https://www.saucedemo.com/checkout-complete.html'


# TEST 9
def test_correct_price_at_checkout(browser):
    """ Given """
    browser = signin(browser)

    total_sum = 0
    first_name = "Peter"
    second_name = "Hamfelt"
    zip_nr = 1337

    """ When """
    # Select and add items to cart
    browser.find_element_by_xpath('//*[@id="add-to-cart-sauce-labs-backpack"]').click()
    browser.find_element_by_xpath('//*[@id="add-to-cart-sauce-labs-bike-light"]').click()

    # View cart
    browser.find_element_by_xpath('//*[@id="shopping_cart_container"]/a').click()

    # Get cart prices
    current_cost = browser.find_elements_by_class_name('inventory_item_price')
    for value in current_cost:
        total_sum += float(value.text[1:])  # Remove $ and convert to float

    # Continue to checkout
    browser.find_element_by_xpath('//*[@id="checkout"]').click()

    # Input checkout information
    browser.find_element_by_xpath('//*[@id="first-name"]').send_keys(first_name)
    browser.find_element_by_xpath('//*[@id="last-name"]').send_keys(second_name)
    browser.find_element_by_xpath('//*[@id="postal-code"]').send_keys(zip_nr)

    # Submit checkout information
    browser.find_element_by_xpath('//*[@id="continue"]').click()

    """ Then """
    # verify correct url
    assert browser.current_url == 'https://www.saucedemo.com/checkout-step-two.html'

    # verify correct total cost excluding tax
    checkout_cost = browser.find_element_by_xpath("//div[@class='summary_subtotal_label']").text
    # Extract float from string
    checkout_cost = re.findall("\d+\.\d+", checkout_cost)
    assert float(checkout_cost[0]) == total_sum


# TEST 10
def test_cancel_checkout(browser):
    """ Given """
    browser = signin(browser)

    first_name = "Peter"
    second_name = "Hamfelt"
    zip_nr = 1337

    """ When """
    # Select and add items to cart
    browser.find_element_by_xpath('//*[@id="add-to-cart-sauce-labs-backpack"]').click()
    browser.find_element_by_xpath('//*[@id="add-to-cart-sauce-labs-bike-light"]').click()

    # View cart
    browser.find_element_by_xpath('//*[@id="shopping_cart_container"]/a').click()

    # Continue to checkout
    browser.find_element_by_xpath('//*[@id="checkout"]').click()

    # Input checkout information
    browser.find_element_by_xpath('//*[@id="first-name"]').send_keys(first_name)
    browser.find_element_by_xpath('//*[@id="last-name"]').send_keys(second_name)
    browser.find_element_by_xpath('//*[@id="postal-code"]').send_keys(zip_nr)

    # Submit checkout information
    browser.find_element_by_xpath('//*[@id="continue"]').click()

    # Cancel checkout overview
    browser.find_element_by_xpath('//*[@id="cancel"]').click()

    """ Then """
    # Check nr of added items are still the same nr
    nr_of_added_items = len(browser.find_elements_by_xpath("//*[contains(text(), 'Remove')]"))
    assert nr_of_added_items == 2

    # Check current browser to be product page
    assert browser.current_url == 'https://www.saucedemo.com/inventory.html'
